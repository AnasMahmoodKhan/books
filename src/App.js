import React  from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import BookPage from './components/book/BookPage';
import About from './components/common/AboutPage';
import Home from './components/common/HomePage';
import Nav from './components/common/Nav';


const App = (props) => {
  return (
    <div className="container">
    <BrowserRouter>
      <Nav />
      <Switch>
        <Route path="/about" component={About} />
        <Route path="/books" component={BookPage} />
        <Route path="/" component={Home} />
      </Switch>
    </BrowserRouter>

    </div>
  );
};

export default App