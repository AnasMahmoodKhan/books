export const createBook = (book) => {
  return {
    type: "CREATE_BOOK",
    book: book,
  };
};

export const removeBook = (id) => {
  return {
    type: "REMOVE_BOOK",
    id: id,
  };
};
