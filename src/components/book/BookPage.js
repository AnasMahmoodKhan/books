import React from "react";
import { connect } from "react-redux";
import * as BookActions from "../../actions/BookActions";

class Book extends React.Component {
  submitBook(input) {
    this.props.createBook(input);
  }

  deleteBook(e, index) {
    e.preventDefault();
    this.props.removeBook(index);
  }

  render() {
    let titleInput;
    return (
      <div>
        <h3>Books</h3>
        <ul>
          {this.props.books.map((b, i) => (
            <React.Fragment>
              <li key={i}>
                {b.title}{" "}
                <button
                  className="btn btn-danger btn-sm"
                  onClick={(e) => this.deleteBook(e, i)}
                >
                  Remove
                </button>
              </li>
            </React.Fragment>
          ))}
        </ul>
        <div>
          <h3>Books Form</h3>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              var input = { title: titleInput.value };
              this.submitBook(input);
              e.target.reset();
            }}
          >
            <input
              type="text"
              name="title"
              ref={(node) => (titleInput = node)}
            />
            <input type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
  return {
    // You can now say this.props.books
    books: state.books,
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    // You can now say this.props.createBook
    createBook: (book) => dispatch(BookActions.createBook(book)),
    removeBook: (index) => dispatch(BookActions.removeBook(index)),
  };
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(Book);
