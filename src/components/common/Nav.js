import React from "react";
import { Link } from "react-router-dom";

const Nav = (props) => {
    console.log(props)
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">
        Books
      </a>
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class={window.location.pathname==="/" ? "nav-item active" : "nav-item" }>
            <Link className="nav-link" to="/">
              Home
            </Link>
          </li>
          <li class={window.location.pathname==="/about" ? "nav-item active" : "nav-item" }>
            <Link className="nav-link" to="/about">
              About
            </Link>
          </li>
          <li class={window.location.pathname==="/books" ? "nav-item active" : "nav-item" }>
            <Link className="nav-link" to="/books">
              Book
            </Link>
          </li>
          <li class={window.location.pathname==="/cart" ? "nav-item active" : "nav-item" }>
            <Link className="nav-link" to="/cart">
              Cart
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Nav;
