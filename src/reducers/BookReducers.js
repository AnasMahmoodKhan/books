/* eslint-disable import/no-anonymous-default-export */
export default (state = [], action) => {
    switch (action.type){
      case 'CREATE_BOOK':
          return [
            ...state,
            Object.assign({}, action.book)
          ];

          case 'REMOVE_BOOK':
            return state.filter((data, i) => i !== action.id);
      default:
            return state;
    }
  };